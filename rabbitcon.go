package main

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/Edwinh4378/etcd-agent/internal/rabbit"
	etcddata "bitbucket.org/Edwinh4378/etcd-data"
	"github.com/streadway/amqp"
)

var connn rabbit.Conn
var ch chan []byte

func rabbitmain() {
	go rabbitmainTSS()
	conn, err := rabbit.GetConn("amqp://user:password@172.17.0.4")
	if err != nil {

		panic(err)
	}
	connn = conn
	// go func() {
	// 	for {
	// 		time.Sleep(time.Second)
	connnTSS.Publish("etcd.ticket.tss", []byte(`{"message":"test"}`))
	// 	}
	// }()

	err = connn.StartConsumer("etcd_tickets", "1184.etcd-tco", handler, 2)

	if err != nil {
		panic(err)
	}

	forever := make(chan bool)
	<-forever

}

func handler(d amqp.Delivery) bool {

	if d.Body == nil {
		fmt.Println("Error, no message body!")
		return false
	}
	// fmt.Println(string(d.Body))
	fmt.Println("ETCD ticket message received")
	var etcdQJsonToData etcddata.TicketMessageQue

	json.Unmarshal(d.Body, &etcdQJsonToData)
	//translate to TSS format
	TSSticketq := translateTSS(etcdQJsonToData)
	fmt.Println(string(TSSticketq))
	//publish to tss

	return true
}
