package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	etcddata "bitbucket.org/Edwinh4378/etcd-data"
	tssdata "bitbucket.org/Edwinh4378/tss-data"

	"github.com/dgrijalva/jwt-go"
)

type Credentials struct {
	Password string `json:"password"`
	Username string `json:"username"`
}

type LoginResponse struct {
	AccessToken  string `json:"AccessToken"`
	ExpiresIn    int64  `json:"ExpiresIn"`
	TokenType    string `json:"TokenType"`
	RefreshToken string `json:"RefreshToken"`
	IDToken      string `json:"IdToken"`
}

type Claims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

var iDtoken string

func getCredentials() {

	requestdata := Credentials{}
	requestdata.Username = "user1"
	requestdata.Password = "password1"
	byteRep, err := json.Marshal(requestdata)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(string(byteRep))

	request, err := http.NewRequest("POST", "http://172.17.0.4:8000/login", bytes.NewBuffer(byteRep))
	if err != nil {
		log.Fatalln(err)
	}

	client := &http.Client{Timeout: time.Second * 10}
	// Send request
	response, err := client.Do(request)
	if err != nil {
		log.Fatal("Error reading response. ", err)
	}
	defer response.Body.Close()

	responseLogin := LoginResponse{}
	json.NewDecoder(response.Body).Decode(&responseLogin)
	//set token env
	iDtoken = responseLogin.IDToken

}

func tokenIsValid() bool {
	claims := &Claims{}
	jwtKey := []byte("notknownatclientside")

	_, _ = jwt.ParseWithClaims(iDtoken, claims, func(token *jwt.Token) (interface{}, error) {

		return jwtKey, nil
	})

	if claims.ExpiresAt < time.Now().Unix() {
		fmt.Println("IDToken is expired")
		return false
	}
	return true
}

func callRetrieveticket(allocator, bookingid string) *etcddata.TicketMessage {
	requestdata := etcddata.ReqRetrieveTicket{
		RequestingRailway: "1184",
		// Carrier:           "1184",
		BookingReference: etcddata.GeneralBookingReference{
			Allocator: allocator,
			BookingID: bookingid}}

	byteRep, err := json.Marshal(requestdata)
	if err != nil {
		log.Fatalln(err)
	}

	bearerToken := "Bearer " + iDtoken
	// fmt.Println(bearerToken)
	req, err := http.NewRequest("POST", "http://172.17.0.4:8000/retrieveticket", bytes.NewBuffer(byteRep))

	req.Header.Add("Authorization", bearerToken)
	fmt.Println(req.Header)
	if err != nil {
		log.Fatalln(err)
	}
	client := &http.Client{Timeout: time.Second * 10}
	// Send request
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Error reading response. ", err)
	}
	defer resp.Body.Close()

	response := etcddata.TicketMessage{}
	json.NewDecoder(resp.Body).Decode(&response)

	return &response
}

func string2int(astring string) int {
	i, _ := strconv.Atoi(astring)

	return i
}

func setvalidfromtill(ticketdata etcddata.TicketPayload) time.Time {
	if ticketdata.TicketReference.DepartureDate.IsZero() {
		return ticketdata.IssuingDetails.IssuingDate
	} else {
		return ticketdata.TicketReference.DepartureDate
	}

}

func retrieveTicketCall() {
	if iDtoken == "" || tokenIsValid() == false {
		getCredentials()
	}

	// callRetrieveticket("0080", "TMRSCJN-1")

}

func translateTSS(ticketdata etcddata.TicketMessageQue) []byte {

	// ticketAll := callRetrieveticket("0080", "TMRSCJN-1")
	ticketAll := ticketdata
	ticketData := etcddata.ReadTicketPayload(ticketAll.Ticket.TicketData.SignedAnonymousTicketData.Payload)

	ticketToTSS := tssdata.TSSticket{
		Issuer:   string2int(ticketData.IssuingDetails.Issuer),
		TicketID: ticketData.TicketReference.BookingID,

		ValidFrom:   setvalidfromtill(*ticketData),
		ValidTill:   setvalidfromtill(*ticketData), // identiek aan ValidFrom vanuit etcd
		IssuingDate: ticketData.IssuingDetails.IssuingDate,
		// TicketRawData:      "",
		// ProcessingDatetime: time.Time{},
		// CertificateVersion: "",
		Annotations: []tssdata.Annotations{},
	}

	for _, val := range ticketAll.Ticket.Annotation {
		// fmt.Println(val)
		// fmt.Println("=============")
		currentAnnotation, annotationType := etcddata.ReadAnnotationPayload(val.Payload)

		// fmt.Println(annotationType, currentAnnotation)
		switch {
		// Control
		case annotationType == "Control":
			{

				cast := currentAnnotation.(*etcddata.ControlAnnotation)

				annotationTSS := tssdata.SetAnnotationControl(
					cast.ID,
					cast.Id,
					cast.Timecreated,
					cast.Control.Result,
					cast.Controller.DeviceID,
					cast.Controller.DeviceType,
					"", //inspectionStaffId
					cast.Controller.TCO,
					"",
					cast.Location.OnRoute.FromStation.LocalCode,
					cast.Location.Train)

				ticketToTSS.Annotations = append(ticketToTSS.Annotations, *annotationTSS)
			}
		// Cancellation
		case annotationType == "Cancellation":
			{
				cast := currentAnnotation.(*etcddata.CancellationAnnotation)
				annotationTSS := tssdata.SetAnnotationCancelation(
					cast.ID,
					cast.Id,
					cast.Cancellation.CancellationTime,
				)
				ticketToTSS.Annotations = append(ticketToTSS.Annotations, *annotationTSS)
			}
		}

	}

	jsonresponse, err := json.Marshal(ticketToTSS)
	if err != nil {
		panic(err)
	}

	// fmt.Println(AnnotationPayload)
	return jsonresponse

}

func main() {
	rabbitmain()
	connnTSS.Publish("etcd.ticket.tss", []byte("sdffsd"))

}
