package main

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/Edwinh4378/etcd-agent/internal/rabbit"
	etcddata "bitbucket.org/Edwinh4378/etcd-data"
	"github.com/streadway/amqp"
)

var connnTSS rabbit.Conn

func rabbitmainTSS() {
	fmt.Println("hi")
	conn, err := rabbit.GetConn("amqp://user:password@172.17.0.4")
	if err != nil {

		panic(err)
	}
	connnTSS = conn
	// go func() {
	// 	for {
	// 		time.Sleep(time.Second)
	// 		conn.Publish("test-key", []byte(`{"message":"test"}`))
	// 	}
	// }()

	// err = connnTSS.StartConsumer("etcd_tickets", "1184.etcd-tco", handler, 2)

	// if err != nil {
	// 	panic(err)
	// }

	// forever := make(chan bool)
	// <-forever

}

func handlerTSS(d amqp.Delivery) bool {

	if d.Body == nil {
		fmt.Println("Error, no message body!")
		return false
	}
	// fmt.Println(string(d.Body))
	fmt.Println("ETCD ticket message received")
	var etcdQJsonToData etcddata.TicketMessageQue

	json.Unmarshal(d.Body, &etcdQJsonToData)
	//translate to TSS format
	TSSticketq := translateTSS(etcdQJsonToData)

	//publish to tss
	err := connnTSS.Publish("etcd.ticket.tss", TSSticketq)
	if err != nil {
		fmt.Println("message NOT published to tss2")
	} else {
		fmt.Println("message published to tss2")
	}

	return true
}
