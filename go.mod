module bitbucket.org/Edwinh4378/etcd-agent

go 1.15

require (
	bitbucket.org/Edwinh4378/etcd-data v0.0.0-20201223202347-edce6385cdd4
	bitbucket.org/Edwinh4378/tss-data v0.0.0-20201214093920-dad177cb7b03
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/streadway/amqp v1.0.0
)
